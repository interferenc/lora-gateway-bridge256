package main

import "gitlab.com/interferenc/lora-gateway-bridge256/cmd/lora-gateway-bridge/cmd"

//go:generate ./doc.sh

var version string // set by the compiler

func main() {
	cmd.Execute(version)
}
